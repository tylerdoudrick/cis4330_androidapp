package com.example.knockknockalarm;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collections;

public class SensorLogger {
    private ArrayList<Float> SensorLogX;
    private ArrayList<Float> SensorLogY;
    private ArrayList<Float> SensorLogZ;
    int count = 0;

    private String sensorType;
    public SensorLogger(String type){
        sensorType = type;
        SensorLogX = new ArrayList<Float>();
        SensorLogY = new ArrayList<Float>();
        SensorLogZ = new ArrayList<Float>();
    }

    public void logSensor(float x, float y, float z){
        SensorLogX.add(x);
        SensorLogY.add(y);
        SensorLogZ.add(z);

        count++;
    }

    public float[] getAVG(){
        float[] totals = {0, 0, 0};
        float[] averages = {0, 0, 0};
        for(int i = 0; i<count; i++){
            totals[0] += SensorLogX.get(i);
            totals[1] += SensorLogY.get(i);
            totals[2] += SensorLogZ.get(i);
        }
        for(int i = 0; i<averages.length; i++){
            averages[i] = totals[i]/count;
        }
        return averages;
    }

    public int getSize(){
        return count;
    }

    public void sort(){
        Collections.sort(SensorLogX);
        Collections.sort(SensorLogY);
        Collections.sort(SensorLogZ);
    }


    private void calcFirstQuartile(){

    }


    private void calcThirdQuartile(){

    }
//10
//0 1 2 3 4 5 6 7 8 9

    //5 AND 6
    private void calcMedian(){
        if(count % 2 == 0){
            int half = count/2;
            float one = SensorLogZ.get(half);
            float two = SensorLogZ.get(half+1);
            float median = (one+two)/2;
        }else{

        }
    }


    public void clearAll(){
        SensorLogX.clear();
        SensorLogY.clear();
        SensorLogZ.clear();
    }
}
